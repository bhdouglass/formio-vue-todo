import Vue from 'vue';
import Router from 'vue-router';

import List from '@/components/List';
import Edit from '@/components/Edit';
import Login from '@/components/Login';

Vue.use(Router);

export default new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'list',
            component: List,
        },
        {
            path: '/login',
            name: 'login',
            component: Login,
        },
        {
            path: '/new',
            name: 'new',
            component: Edit,
        },
        {
            path: '/edit/:id',
            name: 'edit',
            component: Edit,
        },
    ],
});
