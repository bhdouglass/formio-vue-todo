const PROJECT_URL = 'https://mzixvbtyrkrpfwl.form.io';

export default {
    projectUrl: PROJECT_URL,
    forms: {
        login: `${PROJECT_URL}/user/login`,
        register: `${PROJECT_URL}/user/register`,
        todo: `${PROJECT_URL}/todo/submission`,
    },
};
