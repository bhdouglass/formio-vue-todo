import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'choices.js/assets/styles/css/choices.min.css';
import Vue from 'vue';
import VModal from 'vue-js-modal';

import App from './App';
import router from './router';
import auth from './auth';

Vue.config.productionTip = false;
Vue.use(VModal);

/* eslint-disable no-new */
new Vue({
    el: '#app',
    router,
    data: auth.state,
    render: (h) => {
        return h(App);
    },
});
