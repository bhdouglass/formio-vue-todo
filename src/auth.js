import Formiojs from 'formiojs';

let state = {
    currentUser: null,
    authenticated: false,
};

export default {
    state: state,
    updateCurrentUser() {
        return Formiojs.currentUser().then((user) => {
            if (user) {
                state.currentUser = user;
                state.authenticated = true;
            }
            else {
                state.currentUser = null;
                state.authenticated = false;
            }

            return state.currentUser;
        });
    },
    logout() {
        return Formiojs.logout().then(() => {
            state.currentUser = null;
            state.authenticated = false;
        });
    },
};
